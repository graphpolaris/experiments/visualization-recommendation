from enum import Enum

class VisualizationType(Enum):
  NODE_LINK = "Node-Link"
  SEMANTIC_SUBSTRATES = "Semantic Substrates"
  ATTRIBUTE_DRIVEN_NODELINK = "Attribute Driven Positioning"
  ADJACENCY_MATRIX = "Adjacency Matrix"
  QUILTS = "Quilts"
  BIOFABRIC = "BioFabric"
  TREEMAP = "Treemap"
  SUNBURST = "Sunburst"